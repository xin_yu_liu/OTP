package TOTP;


import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.GenericSignatureFormatError;
import java.lang.reflect.UndeclaredThrowableException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class TOTP {
    TOTP() {
    }

    /**
     * 该方法使用JCE保护密码算法。
     * HMAC计算一个HASH消息认证码作为参数
     *
     * @param crypto:密码算法（HmacSHA1, HmacSHA256, HmacSHA512）
     * @param keyBytes:用于HMAC       key
     * @param text:被认证的消息
     */
    private static byte[] hmac_sha(String crypto, byte[] keyBytes, byte[] text) {
        try {
            Mac hmac;
            hmac = Mac.getInstance(crypto);
            SecretKeySpec macKey = new SecretKeySpec(keyBytes, "RAW");
            hmac.init(macKey);
            return hmac.doFinal(text);
        } catch (GeneralSecurityException gse) {
            throw new UndeclaredThrowableException(gse);
        }
    }

    /**
     * 这个方法将16进制字符串转化为bit流
     *
     * @param hex:16进制字符串
     * @return:一个byte数组
     */
    private static byte[] hexStr2Bytes(String hex) {
        //考虑开头为0的字节
        byte[] bArray = new BigInteger("10" + hex, 16).toByteArray();

        byte[] ret = new byte[bArray.length - 1];
        System.arraycopy(bArray, 1, ret, 0, ret.length);
        return ret;
    }

    private static final int[] DIGITS_POWER
            //0  1  2   3    4     5      6       7        8
            = {1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000};

    /**
     * 该方法根据给定的参数生成一个TOTP值
     *
     * @param key:16进制编码的共享密钥
     * @param time:代表时间的值
     * @param returnDigits:返回的数字
     * @return: 以10进制数值字符串，包括digits
     */
    public static String generateTOTP(String key, String time, String returnDigits) {
        return generateTOTP(key, time, returnDigits, "HmacSHA1");
    }

    /**
     * 该方法根据给定的参数生成一个TOTP值
     *
     * @param key:16进制编码的共享密钥
     * @param time:代表时间的值
     * @param returnDigits:返回的数字
     * @param crypto:使用的加密函数
     *
     * @return: 以10进制数值字符串，包括digits
     */
    protected static String generateTOTP(String key, String time, String returnDigits, String crypto) {
        int codeDigits = Integer.decode(returnDigits);
        String result = null;
        //使用计数器
        //前8bytes作为放大系数
        //兼容RFC 4226 (HOTP)

        while(time.length() < 16) time = "0" + time;

        //从一个Byte[]中获取16进制
        byte[] msg = hexStr2Bytes(time);
        byte[] k = hexStr2Bytes(key);
        byte[] hash = hmac_sha(crypto, k, msg);

        //将选定的bytes放入result
        int offset = hash[hash.length-1] & 0xf;

        int binary =
                ((hash[offset] & 0x7f) << 24)|
                ((hash[offset+1] & 0xff) << 16)|
                ((hash[offset+2] & 0xff) << 8)|
                ((hash[offset+3] & 0xff));

        int otp = binary % DIGITS_POWER[codeDigits];

        result = Integer.toString(otp);
        while(result.length() < codeDigits){
            result = "0" + result;
        }
        return result;
    }

    //@TODO 补充一些网络函数

    public static void main(String[] args) {
        // Seed for HMAC-SHA1 - 20 bytes
        String seed = "3132333435363738393031323334353637383930";
        // Seed for HMAC-SHA256 - 32 bytes
        String seed32 = "3132333435363738393031323334353637383930" +
                "313233343536373839303132";
        // Seed for HMAC-SHA512 - 64 bytes
        String seed64 = "3132333435363738393031323334353637383930" +
                "3132333435363738393031323334353637383930" +
                "3132333435363738393031323334353637383930" +
                "31323334";
        long T0 = 0;
        long X = 30;
/*        long testTime[] = {59L, 1111111109L, 1111111111L,
//                1234567890L, 2000000000L, 20000000000L};

//        String steps = "0";
//        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        df.setTimeZone(TimeZone.getTimeZone("UTC"));
//
//        try {
//            System.out.println(
//                    "+---------------+-----------------------+" +
//                            "------------------+--------+--------+");
//            System.out.println(
//                    "|  Time(sec)    |   Time (UTC format)   " +
//                            "| Value of T(Hex)  |  TOTP  | Mode   |");
//            System.out.println(
//                    "+---------------+-----------------------+" +
//                            "------------------+--------+--------+");
//
//            for (int i=0; i<testTime.length; i++) {
//                long T = (testTime[i] - T0)/X;
//                steps = Long.toHexString(T).toUpperCase();
//                while (steps.length() < 16) steps = "0" + steps;
//                String fmtTime = String.format("%1$-11s", testTime[i]);
//                String utcTime = df.format(new Date(testTime[i]*1000));
//                System.out.print("|  " + fmtTime + "  |  " + utcTime +
//                        "  | " + steps + " |");
//                System.out.println(generateTOTP(seed, steps, "8",
//                        "HmacSHA1") + "| SHA1   |");
//                System.out.print("|  " + fmtTime + "  |  " + utcTime +
//                        "  | " + steps + " |");
//                System.out.println(generateTOTP(seed32, steps, "8",
//                        "HmacSHA256") + "| SHA256 |");
//                System.out.print("|  " + fmtTime + "  |  " + utcTime +
//                        "  | " + steps + " |");
//                System.out.println(generateTOTP(seed64, steps, "8",
//                        "HmacSHA512") + "| SHA512 |");
//
//                System.out.println(
//                        "+---------------+-----------------------+" +
//                                "------------------+--------+--------+");
//            }
//        }catch (final Exception e){
//            System.out.println("Error : " + e);
//        }
*/
    //@TODO 直接在main函数中绘制界面

/*        JFrame frame = new JFrame("TOTP");
        frame.setDefaultLookAndFeelDecorated(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setBounds(600,300,500,400);

        JTextField text = new JTextField("_ _ _ _ _ _ ");
        text.setHorizontalAlignment(JTextField.CENTER);
        text.setEditable(false);
        text.setBounds(800,500,400,300);
        text.setFont(new Font("楷体", Font.BOLD, 30));
        frame.add(text);



        frame.setVisible(true);
        text.setVisible(true);

 */

        JFrame frame=new JFrame("TOTP登录器");    //创建Frame窗口
        JPanel jp=new JPanel();    //创建面板

        JTextField txtfield=new JTextField("__ __ __ __ __ __");
        txtfield.setFont(new Font("楷体",Font.BOLD,30));
        txtfield.setHorizontalAlignment(JTextField.CENTER);    //居中对齐
        txtfield.setEditable(false);
        jp.add(txtfield);
        frame.add(jp);
        frame.setBounds(300,200,450,360);


        JButton button = new JButton("登录");
        jp.add(button);
        button.setFont(new Font("黑体",Font.BOLD,16));
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Date date = new Date(System.currentTimeMillis());
                long T = (date.getTime() - T0)/X;
                String steps = "0";
                steps = Long.toHexString(T).toUpperCase();
                txtfield.setText(generateTOTP(seed32, steps, "6", "HmacSHA256"));
            }
        });


        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);



    }


}
