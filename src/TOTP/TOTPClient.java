package TOTP;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Date;

public class TOTPClient extends TOTP{

    public static boolean sent(JTextField textField) {

        try {
            Socket s = new Socket("127.0.0.1", 8888);

            // 打开输出流
            OutputStream os = s.getOutputStream();

            // 发送数字110到服务端
            os.write(110);
            os.close();

            s.close();
            return true;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            textField.setText("ERROR!");
            e.printStackTrace();
            return false;

        }
    }
    public static void main(String[] args) {
        // Seed for HMAC-SHA1 - 20 bytes
        String seed = "3132333435363738393031323334353637383930";
        // Seed for HMAC-SHA256 - 32 bytes
        String seed32 = "3132333435363738393031323334353637383930" +
                "313233343536373839303132";
        // Seed for HMAC-SHA512 - 64 bytes
        String seed64 = "3132333435363738393031323334353637383930" +
                "3132333435363738393031323334353637383930" +
                "3132333435363738393031323334353637383930" +
                "31323334";
        long T0 = 0;
        long X = 30;
/*        long testTime[] = {59L, 1111111109L, 1111111111L,
//                1234567890L, 2000000000L, 20000000000L};

//        String steps = "0";
//        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        df.setTimeZone(TimeZone.getTimeZone("UTC"));
//
//        try {
//            System.out.println(
//                    "+---------------+-----------------------+" +
//                            "------------------+--------+--------+");
//            System.out.println(
//                    "|  Time(sec)    |   Time (UTC format)   " +
//                            "| Value of T(Hex)  |  TOTP  | Mode   |");
//            System.out.println(
//                    "+---------------+-----------------------+" +
//                            "------------------+--------+--------+");
//
//            for (int i=0; i<testTime.length; i++) {
//                long T = (testTime[i] - T0)/X;
//                steps = Long.toHexString(T).toUpperCase();
//                while (steps.length() < 16) steps = "0" + steps;
//                String fmtTime = String.format("%1$-11s", testTime[i]);
//                String utcTime = df.format(new Date(testTime[i]*1000));
//                System.out.print("|  " + fmtTime + "  |  " + utcTime +
//                        "  | " + steps + " |");
//                System.out.println(generateTOTP(seed, steps, "8",
//                        "HmacSHA1") + "| SHA1   |");
//                System.out.print("|  " + fmtTime + "  |  " + utcTime +
//                        "  | " + steps + " |");
//                System.out.println(generateTOTP(seed32, steps, "8",
//                        "HmacSHA256") + "| SHA256 |");
//                System.out.print("|  " + fmtTime + "  |  " + utcTime +
//                        "  | " + steps + " |");
//                System.out.println(generateTOTP(seed64, steps, "8",
//                        "HmacSHA512") + "| SHA512 |");
//
//                System.out.println(
//                        "+---------------+-----------------------+" +
//                                "------------------+--------+--------+");
//            }
//        }catch (final Exception e){
//            System.out.println("Error : " + e);
//        }
*/
/*        JFrame frame = new JFrame("TOTP");
        frame.setDefaultLookAndFeelDecorated(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setBounds(600,300,500,400);

        JTextField text = new JTextField("_ _ _ _ _ _ ");
        text.setHorizontalAlignment(JTextField.CENTER);
        text.setEditable(false);
        text.setBounds(800,500,400,300);
        text.setFont(new Font("楷体", Font.BOLD, 30));
        frame.add(text);



        frame.setVisible(true);
        text.setVisible(true);

 */

        JFrame frame=new JFrame("TOTP登录器");    //创建Frame窗口
        JPanel jp=new JPanel();    //创建面板

        JTextField txtfield=new JTextField("__ __ __ __ __ __");
        txtfield.setFont(new Font("楷体",Font.BOLD,30));
        txtfield.setHorizontalAlignment(JTextField.CENTER);    //居中对齐
        txtfield.setEditable(false);
        jp.add(txtfield);
        frame.add(jp);
        frame.setBounds(300,200,450,360);


        JButton button = new JButton("登录");
        jp.add(button);
        button.setFont(new Font("黑体",Font.BOLD,16));
        button.addActionListener(e -> {
            if(sent(txtfield)){
            Date date = new Date(System.currentTimeMillis());
            long T = (date.getTime() - T0)/X;
            String steps;
            steps = Long.toHexString(T).toUpperCase();
            txtfield.setText(generateTOTP(seed32, steps, "6", "HmacSHA256"));}
        });
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
