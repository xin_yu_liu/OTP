package TOTP;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.Scanner;

public class TOTPServer extends TOTP{
    public static void main(String[] args) {
        // Seed for HMAC-SHA1 - 20 bytes
        String seed = "3132333435363738393031323334353637383930";
        // Seed for HMAC-SHA256 - 32 bytes
        String seed32 = "3132333435363738393031323334353637383930" +
                "313233343536373839303132";
        // Seed for HMAC-SHA512 - 64 bytes
        String seed64 = "3132333435363738393031323334353637383930" +
                "3132333435363738393031323334353637383930" +
                "3132333435363738393031323334353637383930" +
                "31323334";
        long T0 = 0;
        long X = 30;
        boolean OPEN = true;
        boolean CLOSE = false;
        boolean State = true;
        while(State == OPEN) {
            try {

                ServerSocket ss = new ServerSocket(8888);

                System.out.println("监听在端口号:8888");
                Socket s = ss.accept();

                //打开输入流
                InputStream is = s.getInputStream();

                //读取客户端发送的数据

                int msg = is.read();
                is.close();
                s.close();
                ss.close();
                //打印出来
//            System.out.println(msg);
                if (msg == 110) {
                    Date date = new Date(System.currentTimeMillis());
                    long T = (date.getTime() - T0) / X;
                    String steps = "0";
                    steps = Long.toHexString(T).toUpperCase();
                    String key = generateTOTP(seed32, steps, "6", "HmacSHA256");
                    Scanner in = new Scanner(System.in);
                    System.out.println("请输入口令");
                    String input = in.next();
                    if (input.equals(key)) {
                        System.out.println("成功登录！");
                    } else {
                        System.out.println("Forbidden!");
                    }
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}
